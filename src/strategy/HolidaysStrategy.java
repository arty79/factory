package strategy;

/**
 * Created by Artem Panasyuk on 05.05.2017.
 */
public interface HolidaysStrategy {

    void celebrate();

}
