package facade;

/**
 * Created by Artem Panasyuk on 04.05.2017.
 */
public interface Inspector {

    void getPaysFromVk();

    void getPaysFromFb();
}
