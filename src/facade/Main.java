package facade;

/**
 * Created by Artem Panasyuk on 04.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        AglyInspector inspector = new AglyInspector();
        inspector.makeBrief();
    }
}
