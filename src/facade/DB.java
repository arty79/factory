package facade;

import java.util.List;

/**
 * Created by Artem Panasyuk on 04.05.2017.
 */
public interface DB {

    List<String> getListForFriends();

    int getMoney();

    List<String> getWall();
}
