package facade;

/**
 * Created by Artem Panasyuk on 04.05.2017.
 */
public class AglyInspector implements Inspector, Detective{

    @Override
    public void makeBrief() {
        VKSocialWeb.getHistory();
        VKSocialWeb.getLikes(1, 10);
        FaceBook.getHistory("10.10.10");
        FaceBook.getLikes(1,true,10);
        System.out.println("brief complete");
    }

    @Override
    public void getPaysFromVk() {
        System.out.println("pays from VK");
    }

    @Override
    public void getPaysFromFb() {
        System.out.println("pays from fb");
    }
}
