package factory;

/**
 * Created by Artem Panasyuk on 03.05.2017.
 */
public interface WeaponFactory {
    Gun createGun();
    Revolver createRevolver();
    Rifle createRifle();
    BFG createBFG();
}
