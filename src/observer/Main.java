package observer;

/**
 * Created by Artem Panasyuk on 04.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        HR hr = new HR();
        Worker worker = new Worker(1);
        Worker worker2 = new Worker(2);
        hr.registerObserver(worker);
        hr.registerObserver(worker2);
        hr.notifyAllObservers();
    }
}
