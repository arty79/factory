package observer;

/**
 * Created by Artem Panasyuk on 04.05.2017.
 */
public interface Observer {

    void message(String message);
}
