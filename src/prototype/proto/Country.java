package prototype.proto;

import java.util.List;

/**
 * Created by Artem Panasyuk on 03.05.2017.
 */
public class Country implements Cloneable {

    private List<House> houses;
    private List<District> districts;
    private List<City> cities;

    @Override
    public Country clone() throws CloneNotSupportedException {
        Country copy = (Country) super.clone();

        //In an actual implementation of this pattern you might now change references to
        //the expensive to produce parts from the copies that are held inside the prototype.

        return copy;
    }

}
