package prototype.proto;

import prototype.details.Kvartal;

import java.util.List;

/**
 * Created by Artem Panasyuk on 03.05.2017.
 */
public class District {
    private int budget;
    List<Kvartal> kvartals;

    /**
     * Getter for property 'budget'.
     *
     * @return Value for property 'budget'.
     */
    public int getBudget() {
        return budget;
    }

    /**
     * Setter for property 'budget'.
     *
     * @param budget Value to set for property 'budget'.
     */
    public void setBudget(int budget) {
        this.budget = budget;
    }
}
