package prototype.proto;

import java.util.List;

/**
 * Created by Artem Panasyuk on 03.05.2017.
 */
public class City {
    private int square;
    List<District> districts;

    /**
     * Getter for property 'square'.
     *
     * @return Value for property 'square'.
     */
    public int getSquare() {
        return square;
    }

    /**
     * Setter for property 'square'.
     *
     * @param square Value to set for property 'square'.
     */
    public void setSquare(int square) {
        this.square = square;
    }
}
