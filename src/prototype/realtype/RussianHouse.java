package prototype.realtype;

import prototype.proto.House;

/**
 * Created by Artem Panasyuk on 03.05.2017.
 */
public class RussianHouse extends House {
    private int peoples;

    /**
     * Getter for property 'peoples'.
     *
     * @return Value for property 'peoples'.
     */
    public int getPeoples() {
        return peoples;
    }

    /**
     * Setter for property 'peoples'.
     *
     * @param peoples Value to set for property 'peoples'.
     */
    public void setPeoples(int peoples) {
        this.peoples = peoples;
    }
}
