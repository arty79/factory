package prototype;

import prototype.proto.Country;
import prototype.realtype.RussianCountry;

/**
 * Created by Artem Panasyuk on 03.05.2017.
 */
public class Main {

    private Country country; // Could have been a private Cloneable country.

    public Main(Country country) {
        this.country = country;
    }

    public Country makeCountry() throws CloneNotSupportedException {
        return (Country) this.country.clone();
    }

    public static void main(String args[]) throws CloneNotSupportedException {
        Country tempCountry = null;
        Country prot = new RussianCountry();
        Main cm = new Main(prot);
        for (int i = 0; i < 2; i++) {
            tempCountry = cm.makeCountry();
        }
        System.out.println(tempCountry);
    }

}