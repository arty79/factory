package builder;

import builder.galustan.Jamshut;
import builder.galustan.Ravshan;
import builder.svetlakov.Boss;

/**
 * Created by Artem Panasyuk on 03.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        HouseBuilder builder = new Ravshan();
        HouseBuilder builder2 = new Jamshut();
        Boss boss = new Boss(builder2);
        boss.build();

    }
}
