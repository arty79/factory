package builder;

/**
 * Created by Artem Panasyuk on 03.05.2017.
 */
public interface HouseBuilder {
    void fillBase();
    void createWalls();
    void createRoof();
    String getResult();
}
