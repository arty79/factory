package statement;

/**
 * Created by Artem Panasyuk on 05.05.2017.
 */
public class StateError extends State {

    public static State Instance() {
        return new StateError();
    }

    @Override
    public void errorDoc(Context conn) {
        super.errorDoc(conn);
        ChangeState(conn, StateError.Instance());
    }

    @Override
    public void sendDoc(Context conn) {
        super.errorDoc(conn);
        ChangeState(conn, StateSended.Instance());
    }


}
