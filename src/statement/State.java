package statement;

/**
 * Created by Artem Panasyuk on 05.05.2017.
 */
public class State {

    public void openDoc(Context conn) {
    }

    public void newDoc(Context conn) {
    }

    public void signDoc(Context conn) {
    }

    public void errorDoc(Context conn) {
    }

    public void regDoc(Context conn) {
    }

    public void validateDoc(Context conn) {
    }

    public void sendDoc(Context conn) {
    }

    public void recieveDoc(Context conn) {
    }

    public void rejectDoc(Context conn) {
    }

    protected void ChangeState(Context conn, State state) {
        conn.ChangeState(state);
    }
}