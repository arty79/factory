package statement;

/**
 * Created by Artem Panasyuk on 05.05.2017.
 */
public class StateNew extends State {

    public static State Instance() {
        return new StateNew();
    }

    @Override
    public void newDoc(Context conn) {
        super.newDoc(conn);
        ChangeState(conn, StateNew.Instance());
    }

}
