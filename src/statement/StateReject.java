package statement;

/**
 * Created by Artem Panasyuk on 05.05.2017.
 */
public class StateReject extends State {

    public static State Instance() {
        return new StateReject();
    }

    @Override
    public void rejectDoc(Context conn) {
        super.rejectDoc(conn);
        ChangeState(conn, StateReject.Instance());
    }

    @Override
    public void sendDoc(Context conn) {
        super.sendDoc(conn);
        ChangeState(conn, StateSended.Instance());
    }


}
