package statement;

/**
 * Created by Artem Panasyuk on 05.05.2017.
 */
public class StateReg extends State {

    public static State Instance() {
        return new StateReg();
    }

    @Override
    public void regDoc(Context conn) {
        super.regDoc(conn);
        ChangeState(conn, StateReg.Instance());
    }

}
