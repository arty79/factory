package statement;

/**
 * Created by Artem Panasyuk on 05.05.2017.
 */
public class Context {
    State state;



    public Context() {
        state = StateOpen.Instance();
    }

    public void newDoc() {
        state.newDoc(this);
    }

    public void sendDoc() {
        state.sendDoc(this);
    }

    public void recieveDoc() {
        state.recieveDoc(this);
    }


    public void signDoc() {
        state.signDoc(this);
    }

    public void errorDoc() {
        state.errorDoc(this);
    }

    public void regDoc() {
        state.regDoc(this);
    }

    public void validateDoc() {
        state.validateDoc(this);
    }


    void ChangeState(State state) {
        this.state = state;
    }
}
