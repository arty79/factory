package statement;

/**
 * Created by Artem Panasyuk on 05.05.2017.
 */
public class StateValidate extends State {

    public static State Instance() {
        return new StateValidate();
    }

    @Override
    public void validateDoc(Context conn) {
        super.validateDoc(conn);
        ChangeState(conn, StateValidate.Instance());
    }

}
