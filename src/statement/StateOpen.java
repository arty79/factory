package statement;

/**
 * Created by Artem Panasyuk on 05.05.2017.
 */
public class StateOpen extends State {

    public static State Instance() {
        return new StateOpen();
    }

    @Override
    public void openDoc(Context conn) {
        super.openDoc(conn);
        ChangeState(conn, StateNew.Instance());
    }

}
