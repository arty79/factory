package statement;

/**
 * Created by Artem Panasyuk on 05.05.2017.
 */
public class StateSended extends State {

    public static State Instance() {
        return new StateSended();
    }

    @Override
    public void sendDoc(Context conn) {
        super.newDoc(conn);
        ChangeState(conn, StateSigned.Instance());
    }

}
