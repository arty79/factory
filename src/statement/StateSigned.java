package statement;

/**
 * Created by Artem Panasyuk on 05.05.2017.
 */
public class StateSigned extends State {

    public static State Instance() {
        return new StateSigned();
    }

    @Override
    public void signDoc(Context conn) {
        super.signDoc(conn);
        ChangeState(conn, StateSended.Instance());
    }

}
