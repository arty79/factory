package decor;

/**
 * Created by Artem Panasyuk on 04.05.2017.
 */
public interface CountTariffInterface {
    void processTarif();
}
