package decor;

/**
 * Created by Artem Panasyuk on 04.05.2017.
 */
public class SmsTariff extends TariffDecorator {

    public SmsTariff(CountTariffInterface decorator) {
        super(decorator);
    }

    @Override
    public void processTarif() {
        System.out.println("100 SMS");
        super.processTarif();
    }
}
