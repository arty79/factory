package decor;

/**
 * Created by Artem Panasyuk on 04.05.2017.
 */
public class TariffDecorator implements CountTariffInterface {

    private CountTariffInterface decorator;

    @Override
    public void processTarif() {
        decorator.processTarif();
    }

    public TariffDecorator(CountTariffInterface decorator) {
        this.decorator = decorator;
    }
}
