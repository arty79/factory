package chain;

/**
 * Created by Artem Panasyuk on 05.05.2017.
 */
public class Main {


    public static void main(String[] args) {
        BabkiRumors babkiRumors = new BabkiRumors();
        babkiRumors.setLive(false);
        babkiRumors.setMessage("String are very poppulary in Innopolice");
        NewspaperRumors newspaperRumors = new NewspaperRumors();
        newspaperRumors.setBalance(11000L);
        InternetRumors internetRumors = new InternetRumors();
        internetRumors.setConnect(false);
        babkiRumors.setRumor(newspaperRumors);
        newspaperRumors.setRumor(internetRumors);
        babkiRumors.obs();
    }
}
