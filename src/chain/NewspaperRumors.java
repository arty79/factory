package chain;

/**
 * Created by Artem Panasyuk on 05.05.2017.
 */
public class NewspaperRumors extends Rumor {

    public void setBalance(long balance) {
        this.balance = balance;
    }

    private long balance=0;

    @Override
    public void obs() {
        if (balance > 10000) {
            System.out.println("Rumor not true");
            return;
        }
        System.out.println("Newspaper writer");
        super.obs();
    }
}
