package adapter;

/**
 * Created by Artem Panasyuk on 04.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        BasicSocialWeb bw = new VKSocialWeb();
        ExtendedSocialWeb ew = new ExtendedSocialWebAdapter(bw);

        System.out.println(ew.getHistory(1, "date today"));
    }
}
