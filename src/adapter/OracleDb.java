package adapter;

import java.util.List;

/**
 * Created by Artem Panasyuk on 04.05.2017.
 */
public class OracleDb implements DB {
    @Override
    public List<String> getListForFriends() {
        return null;
    }

    @Override
    public int getMoney() {
        return 0;
    }

    @Override
    public List<String> getWall() {
        return null;
    }
}
